# STMediaKit

[![CI Status](https://img.shields.io/travis/shilei2015/STMediaKit.svg?style=flat)](https://travis-ci.org/shilei2015/STMediaKit)
[![Version](https://img.shields.io/cocoapods/v/STMediaKit.svg?style=flat)](https://cocoapods.org/pods/STMediaKit)
[![License](https://img.shields.io/cocoapods/l/STMediaKit.svg?style=flat)](https://cocoapods.org/pods/STMediaKit)
[![Platform](https://img.shields.io/cocoapods/p/STMediaKit.svg?style=flat)](https://cocoapods.org/pods/STMediaKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

STMediaKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'STMediaKit'
```

## Author

shilei2015, 244235126@qq.com

## License

STMediaKit is available under the MIT license. See the LICENSE file for more info.
