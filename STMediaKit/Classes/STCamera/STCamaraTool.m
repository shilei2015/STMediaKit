//
//  STCamaraTool.m
//  STTempPods
//
//  Created by 石磊 on 2022/8/4.
//

#import "STCamaraTool.h"

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>


#ifndef dispatch_main_async_safe    //#if not define
#define dispatch_main_async_safe(block)\
if (dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL) == dispatch_queue_get_label(dispatch_get_main_queue())) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}
#endif


@interface STCamaraTool ()<AVCapturePhotoCaptureDelegate,AVCaptureVideoDataOutputSampleBufferDelegate>

/*----------------------分割线------------------------*/
//捕获设备，通常是前置摄像头，后置摄像头，麦克风（音频输入）
@property(nonatomic)AVCaptureDevice *device;

//AVCaptureDeviceInput 代表输入设备，他使用AVCaptureDevice 来初始化
@property(nonatomic)AVCaptureDeviceInput *input;

//session：由他把输入输出结合在一起，并开始启动捕获设备（摄像头）
@property(nonatomic)AVCaptureSession *session;

//图像预览层，实时显示捕获的图像
@property(nonatomic)AVCaptureVideoPreviewLayer *previewLayer;

@property (nonatomic,weak) UIView *localView;



@property (nonatomic) AVCapturePhotoOutput *imageOutPut;


@property (nonatomic , assign) CMPosition position;

@property (nonatomic , strong) UIVisualEffectView * blur;

@end


@implementation STCamaraTool

+ (instancetype)startCameraWithView:(UIView *)view {
    STCamaraTool * tool = [STCamaraTool new];
    tool.localView = view;
    [tool setupLocalCameraShow];
    
    {
        UIVisualEffectView * blur = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:(UIBlurEffectStyleLight)]];
        [view addSubview:blur];
        
        UIView * sV = view;
        UIView * mV = blur;
        
        // 添加 right 约束
        NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:mV attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:sV attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
        [sV addConstraint:rightConstraint];

        // 添加 left 约束
        NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:mV attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:sV attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
        [sV addConstraint:leftConstraint];
        
        // 添加 top 约束
        NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:mV attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:sV attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
        [sV addConstraint:topConstraint];
        
        // 添加 bottom 约束
        NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:mV attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:sV attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
        [sV addConstraint:bottomConstraint];
        
        tool.blur = blur;
        
        blur.hidden = YES;
    }
    
    return tool;
}

- (void)setupLocalCameraShow {
    [self resetLocalCamera];
    //使用AVMediaTypeVideo 指明self.device代表视频，默认使用后置摄像头进行初始化
    
    self.device = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:(AVCaptureDevicePositionFront)];
    
    self.position = CMPositionFront;
    
    //使用设备初始化输入
    self.input = [[AVCaptureDeviceInput alloc] initWithDevice:self.device error:nil];
    
    //生成会话，用来结合输入输出
    self.session = [[AVCaptureSession alloc]init];
    
    if ([self.session canSetSessionPreset:AVCaptureSessionPresetPhoto]) {
        [self.session setSessionPreset:AVCaptureSessionPresetPhoto];
    }
    
    if ([self.session canAddInput:self.input]) {
        [self.session addInput:self.input];
    }
    
    //使用self.session，初始化预览层，self.session负责驱动input进行信息的采集，layer负责把图像渲染显示
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    
    //设置拍摄图层的大小
    double width = self.localView.bounds.size.width;
    double height = self.localView.bounds.size.height;
    
    self.previewLayer.frame = CGRectMake(0, 0, width,height);
    self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.localView.layer insertSublayer:self.previewLayer atIndex:0];
    
    
    //修改设备的属性，先加锁
    if ([self.device lockForConfiguration:nil]) {
        
        //自动白平衡
        if ([self.device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance]) {
            [self.device setWhiteBalanceMode:AVCaptureWhiteBalanceModeAutoWhiteBalance];
        }
        
        //解锁
        [self.device unlockForConfiguration];
    }
    
    self.imageOutPut = [[AVCapturePhotoOutput alloc]init];
    if ([self.session canAddOutput:self.imageOutPut]) {
        [self.session addOutput:self.imageOutPut];
    }
    
    {
        AVCaptureVideoDataOutput * videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
        
        dispatch_queue_t videoDataOutputQueue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
        [videoDataOutput setSampleBufferDelegate:self queue:videoDataOutputQueue];
        [self.session addOutput:videoDataOutput];
        
    }
    
    
    
    AVCaptureConnection *imageConnection = [self.imageOutPut connectionWithMediaType:AVMediaTypeVideo];
    // 设置 imageConnection 控制相机拍摄图片的角度方向
    if (imageConnection.supportsVideoOrientation) {
        imageConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
    }
    
}

- (void)startRun {
    //开始启动
    
    __weak typeof(self) bself = self;
    
    dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    
    dispatch_async(sessionQueue, ^{
        if (!bself.session.running) {
            [bself.session startRunning];
        }
    });
    
}


- (void)resetLocalCamera {
    if (self.device) {
        self.device = nil;
        self.input = nil;
        self.session = nil;
        [self.previewLayer removeFromSuperlayer];
        self.previewLayer = nil;
    }
}


- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position {
    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera]
                                                                                                                            mediaType:AVMediaTypeVideo
                                                                                                                             position:AVCaptureDevicePositionUnspecified];
    NSArray *captureDevices = [captureDeviceDiscoverySession devices];
    
    for (AVCaptureDevice *device in captureDevices )
        if ( device.position == position )
            return device;
    return nil;
}


- (void)transformationCameraPosition:(CMPosition)cusPosition {
    
    self.position = cusPosition;
    
    //获取摄像头的数量（该方法会返回当前能够输入视频的全部设备，包括前后摄像头和外接设备）
    
    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera]
                                                                                                                            mediaType:AVMediaTypeVideo
                                                                                                                             position:AVCaptureDevicePositionUnspecified];
    NSArray *captureDevices = [captureDeviceDiscoverySession devices];
    
    NSInteger cameraCount = [captureDevices count];
    //摄像头的数量小于等于1的时候直接返回
    if (cameraCount <= 1) {return;}
    
    AVCaptureDevice *newDevice = nil;
    AVCaptureDeviceInput *newInput = nil;
    
    //获取当前相机的方向（前/后）
    if (cusPosition == CMPositionBack) {
        newDevice = [self cameraWithPosition:AVCaptureDevicePositionBack];
    } else if (cusPosition == CMPositionFront) {
        newDevice = [self cameraWithPosition:AVCaptureDevicePositionFront];
    }
    
    
    //输入流
    newInput = [AVCaptureDeviceInput deviceInputWithDevice:newDevice error:nil];
    if (newInput != nil) {
        [self.session beginConfiguration];
        //先移除原来的input
        [self.session removeInput:self.input];
        if ([self.session canAddInput:newInput]) {
            [self.session addInput:newInput];
            self.input = newInput;
        } else {
            //如果不能加现在的input，就加原来的input
            [self.session addInput:self.input];
        }
        [self.session commitConfiguration];
    }
    
}

- (void)cameraOn:(BOOL)isOn {
    
    if (self.isMaskType) {
        self.blur.hidden = isOn;
        return;
    }
    
    isOn ? [self.session startRunning] : [self.session stopRunning];
}

- (void)switchCamera {
    if (self.position == CMPositionBack) {
        [self transformationCameraPosition:CMPositionFront];
    } else {
        [self transformationCameraPosition:CMPositionBack];
    }
}


- (void)takePhoto {
    AVCaptureConnection * videoConnection = [self.imageOutPut connectionWithMediaType:AVMediaTypeVideo];
    if (videoConnection ==  nil) {
        return;
    }
    AVCapturePhotoSettings *set = [AVCapturePhotoSettings photoSettings];
    [self.imageOutPut capturePhotoWithSettings:set delegate:self];
}

//MARK: - - - - - - - - - - - AVCapturePhotoCaptureDelegate
- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(NSError *)error {
    if (!error) {
        
        //        NSData *imageData = [photo fileDataRepresentation];
        //        UIImage *image = [UIImage imageWithData:imageData];
        //处理图片
        
    }
}







- (void)releaseResources {
    [self resetLocalCamera];
}




- (void)dealloc {
    [self subDealloc];
    
#if DEBUG
    NSString * logT = [NSString stringWithFormat:@"__dealloc__❤️❤️❤️❤️❤️❤️❤️❤️ %@",NSStringFromClass([self class])];
    NSLog(@"%@",logT);
#endif
}

- (void)subDealloc {}

@end
