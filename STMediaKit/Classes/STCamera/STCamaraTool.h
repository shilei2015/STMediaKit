//
//  STCamaraTool.h
//  STTempPods
//
//  Created by 石磊 on 2022/8/4.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CMPosition) {
    CMPositionFront = 1,
    CMPositionBack = 2,
};

NS_ASSUME_NONNULL_BEGIN

@interface STCamaraTool : NSObject

@property (nonatomic , assign) BOOL isMaskType;

+ (instancetype)startCameraWithView:(UIView *)view;
- (void)startRun;
- (void)transformationCameraPosition:(CMPosition)cusPosition;
- (void)cameraOn:(BOOL)isOn;
- (void)switchCamera;
- (void)releaseResources;


@end

NS_ASSUME_NONNULL_END
