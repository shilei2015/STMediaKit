//
//  STCamaraTool.h
//  STTempPods
//
//  Created by 石磊 on 2022/8/4.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    PlahHandleTypeEnd,
} PlahHandleType;


NS_ASSUME_NONNULL_BEGIN
typedef void(^PlayHandle)(PlahHandleType type);

@interface STPlayerTool : NSObject

+ (instancetype)startPlayerOn:(UIView *)view url:(nullable NSString *)urlstring seekTime:(NSInteger)seekTime;

+ (instancetype)startPlayerOn:(UIView *)view url:(nullable NSString *)urlstring seekTime:(NSInteger)seekTime playHandle:(nullable PlayHandle)handle;

@property(nonatomic, copy) PlayHandle handle;

///是否全部结束
@property(nonatomic, assign, readonly) BOOL isEnd;


///当前时间
@property(nonatomic, assign,readonly) NSInteger playTime;

///是否重复
@property(nonatomic, assign) BOOL isReplay;

///禁用控制默认NO
@property(nonatomic, assign) BOOL disableControl;

///开始
- (void)startRun;

///结束-释放资源
- (void)releaseResources;



@end

NS_ASSUME_NONNULL_END


/**
 
         //6:37
         url = @"https://v-cdn.zjol.com.cn/276976.mp4";
 
         //0:52 开屏是黑色
         url = @"https://media.w3.org/2010/05/sintel/trailer.mp4";
 
         //0:12
         url = @"https://www.w3schools.com/html/movie.mp4";
 
 */
