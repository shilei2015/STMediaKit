//
//  STPlayerTool.m
//  STTempPods
//
//  Created by 石磊 on 2022/8/4.
//

#import "STPlayerTool.h"

#import <AVFoundation/AVFoundation.h>

#define KDefineZFPlayer 1
#if KDefineZFPlayer
#import <ZFPlayer/ZFPlayer.h>
#import <ZFPlayer/ZFAVPlayerManager.h>
#endif



#ifdef DEBUG

#define NSLog(FORMAT, ...) fprintf(stderr,"--------------------------------------------------------------------------\n%s>%s:%d\t%s\n",__TIME__,[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#else

#define NSLog(...)

#endif


@interface STPlayerTool ()

#if KDefineZFPlayer
@property (nonatomic , strong) ZFPlayerController * zfPlayer;
#endif

@property (nonatomic,strong) UIView *contentView;

@property(nonatomic, copy) NSString *urlString;

@property(nonatomic, assign) NSInteger seekTime;

@property(nonatomic, assign,readwrite) NSInteger playTime;

@property(nonatomic, assign) BOOL readyToPlay;

@property(nonatomic, assign, readwrite) BOOL isEnd;

@end


@implementation STPlayerTool

+ (instancetype)startPlayerOn:(UIView *)view url:(nullable NSString *)urlstring seekTime:(NSInteger)seekTime {
    STPlayerTool * tool = [STPlayerTool startPlayerOn:view url:urlstring seekTime:seekTime playHandle:nil];
    return tool;
}

+ (instancetype)startPlayerOn:(UIView *)view url:(nullable NSString *)urlstring seekTime:(NSInteger)seekTime playHandle:(nullable PlayHandle)handle {
    
    STPlayerTool * tool = [STPlayerTool new];
    tool.contentView = view;
    tool.urlString = urlstring;
    tool.seekTime = seekTime;
    tool.handle = handle;
    
    [tool setupPlayer];
    
    return tool;
}

- (NSURL *)mvURL {
    if ([self.urlString hasPrefix:@"http"]) {
        return [NSURL URLWithString:self.urlString];
    } else {
        return [NSURL fileURLWithPath:self.urlString];
    }
}

#if KDefineZFPlayer
- (void)setupPlayer {
    
    ZFAVPlayerManager *playerManager = [[ZFAVPlayerManager alloc] init];
    playerManager.shouldAutoPlay = NO;
    playerManager.seekTime = self.seekTime;
    playerManager.scalingMode = ZFPlayerScalingModeAspectFill;
    playerManager.assetURL = [NSURL URLWithString:self.urlString];
    
    /// 播放器相关
    self.zfPlayer = [ZFPlayerController playerWithPlayerManager:playerManager containerView:self.contentView];
    self.zfPlayer.controlView = nil;
    /// 设置退到后台继续播放
    self.zfPlayer.pauseWhenAppResignActive = NO;
    self.zfPlayer.orientationObserver.supportInterfaceOrientation = ZFInterfaceOrientationMaskPortrait;
    
    
    @zf_weakify(self)
    /// 播放完成
    self.zfPlayer.playerDidToEnd = ^(id  _Nonnull asset) {
        @zf_strongify(self)
        self.isEnd = YES;
        if (self.handle) {self.handle(PlahHandleTypeEnd);}
        [self.zfPlayer stop];
        if ([STPlayerTool enableLog]) {NSLog(@"播放结束");}
    };
    
    __weak typeof(self) bself = self;
    {
        //视频大小改变
        self.zfPlayer.presentationSizeChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, CGSize size) {
            if ([STPlayerTool enableLog]) {NSLog(@"视频大小改变");}
        };
        
        //加载状态改变
        self.zfPlayer.playerLoadStateChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, ZFPlayerLoadState loadState) {
            if ([STPlayerTool enableLog]) {NSLog(@"加载状态改变 【%zd】",loadState);}
        };
        
        //准备完成
        self.zfPlayer.playerReadyToPlay = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSURL * _Nonnull assetURL) {
            
            if ([STPlayerTool enableLog]) {NSLog(@"准备完成");}
        };
        
        
        /// 播放时间变化
        self.zfPlayer.playerPlayTimeChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSTimeInterval currentTime, NSTimeInterval duration) {
            @zf_strongify(self)
            self.playTime = currentTime;
            
            if ([STPlayerTool enableLog]) {NSLog(@"currentTime = %lf, duration = %lf",currentTime,duration);}
        };
        
        //准备播放
        self.zfPlayer.playerPrepareToPlay = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSURL * _Nonnull assetURL) {
            if ([STPlayerTool enableLog]) {NSLog(@"准备播放");}
        };
        
        //缓冲
        self.zfPlayer.playerBufferTimeChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSTimeInterval bufferTime) {
//            NSLog(@"缓冲");
        };
        
        //播放状态改变
        self.zfPlayer.playerPlayStateChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, ZFPlayerPlaybackState playState) {
            
            NSArray * arr = @[@"ZFPlayerPlayStateUnknown",
                              @"ZFPlayerPlayStatePlaying",
                              @"ZFPlayerPlayStatePaused",
                              @"ZFPlayerPlayStatePlayFailed",
                              @"ZFPlayerPlayStatePlayStopped"];
            if ([STPlayerTool enableLog]) {NSLog(@"播放状态改变 >>>>【%@】",arr[playState]);}
            if (playState == ZFPlayerPlayStatePaused) {
                
                [NSObject cancelPreviousPerformRequestsWithTarget:bself];
                [bself performSelector:@selector(startRun) withObject:nil afterDelay:1];
                
            }
        };
        
        
        self.zfPlayer.playerPlayFailed = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, id  _Nonnull error) {
            if ([STPlayerTool enableLog]) {NSLog(@"播放失败");}
        };
        
        
    }
    
    
    /*
     

     /// The block invoked when the player is Prepare to play.
     @property (nonatomic, copy, nullable) void(^playerPrepareToPlay)(id<ZFPlayerMediaPlayback> asset, NSURL *assetURL);

     /// The block invoked when the player is Ready to play.
     @property (nonatomic, copy, nullable) void(^playerReadyToPlay)(id<ZFPlayerMediaPlayback> asset, NSURL *assetURL);

     /// The block invoked when the player play progress changed.
     @property (nonatomic, copy, nullable) void(^playerPlayTimeChanged)(id<ZFPlayerMediaPlayback> asset, NSTimeInterval currentTime, NSTimeInterval duration);

     /// The block invoked when the player play buffer changed.
     @property (nonatomic, copy, nullable) void(^playerBufferTimeChanged)(id<ZFPlayerMediaPlayback> asset, NSTimeInterval bufferTime);

     /// The block invoked when the player playback state changed.
     @property (nonatomic, copy, nullable) void(^playerPlayStateChanged)(id<ZFPlayerMediaPlayback> asset, ZFPlayerPlaybackState playState);

     /// The block invoked when the player load state changed.
     @property (nonatomic, copy, nullable) void(^playerLoadStateChanged)(id<ZFPlayerMediaPlayback> asset, ZFPlayerLoadState loadState);

     /// The block invoked when the player play failed.
     @property (nonatomic, copy, nullable) void(^playerPlayFailed)(id<ZFPlayerMediaPlayback> asset, id error);

     /// The block invoked when the player play end.
     @property (nonatomic, copy, nullable) void(^playerDidToEnd)(id<ZFPlayerMediaPlayback> asset);

     // The block invoked when video size changed.
     @property (nonatomic, copy, nullable) void(^presentationSizeChanged)(id<ZFPlayerMediaPlayback> asset, CGSize size);
     
     */
    
    
    [self.zfPlayer.currentPlayerManager playerPrepareToPlay];
}


- (void)startRun {
    [self.zfPlayer.currentPlayerManager play];
    
}

- (void)releaseResources {
    [self.zfPlayer stop];
    self.zfPlayer.containerView = nil;
    self.zfPlayer = nil;
}



+ (BOOL)enableLog {
    return YES;
}

//- (NSInteger)playTime {
//    if (_playTime == -1) {
//        return -1;
//    } else if (self.zfPlayer) {
//        return self.zfPlayer.currentTime;
//    }
//    return 0;
//}

#else

- (void)setupPlayer {
    
}

#endif




- (void)setDisableControl:(BOOL)disableControl {
    _disableControl = disableControl;
}


- (void)dealloc {
    [self subDealloc];
    NSString * logT = [NSString stringWithFormat:@"__dealloc__❤️❤️❤️❤️❤️❤️❤️❤️ %@",NSStringFromClass([self class])];
    NSLog(@"%@",logT);
}

- (void)subDealloc {}




@end
