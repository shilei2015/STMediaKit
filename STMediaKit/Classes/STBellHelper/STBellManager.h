
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STBellManager : NSObject

/// 调用手机震动
+ (void)vibration; 


/// numberOfLoops = 0 重复播放
+ (void)startBell:(NSString *)sound system:(BOOL)isSystem numberOfLoops:(int)numberOfLoops;

+ (void)stopBell:(NSString *)sound system:(BOOL)isSystem;

@end

NS_ASSUME_NONNULL_END
