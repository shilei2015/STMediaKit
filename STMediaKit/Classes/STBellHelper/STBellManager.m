
#import "STBellManager.h"

#import <AVFoundation/AVFoundation.h>

static SystemSoundID vibrationID = 1520;


@interface STBellManager ()

@property (nonatomic,strong) AVAudioPlayer * player;


@end


@implementation STBellManager


+ (STBellManager *)manager {
    static STBellManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (void)vibration {
    AudioServicesPlaySystemSound(vibrationID);
}

+ (void)startBell:(NSString *)sound system:(BOOL)isSystem numberOfLoops:(int)numberOfLoops {

    NSArray <NSString *>*soundArr = [sound componentsSeparatedByString:@"."];
    NSString * soundName = soundArr.firstObject;
    NSString * soundType = soundArr.lastObject;
    SystemSoundID soundId = [sound intValue];
    
    NSString * path;
    if (isSystem) {
        path = [NSString stringWithFormat:@"/System/Library/Audio/UISounds/%@.%@",soundName,soundType];
    } else {
        path = [[NSBundle mainBundle] pathForResource:soundName ofType:soundType];//铃声
    }

    if (path) {
        NSURL * url = [NSURL fileURLWithPath:path];
        if ([soundType isEqualToString:@"mp3"]) {
            NSError * error;
            [[STBellManager manager].player stop];
            [STBellManager manager].player = nil;

            AVAudioPlayer * _player = [STBellManager manager].player;

            if (!_player) {
                
                AVAudioSession *audioSession = [AVAudioSession sharedInstance];
                
                NSError * error;
                //默认情况下扬声器播放
                [audioSession setCategory:AVAudioSessionCategoryAmbient error:&error];
                [audioSession setActive:YES error:nil];
                
                if (error) {
                    NSLog(@"%@",error);
                }
                
                _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
                _player.numberOfLoops = numberOfLoops;
                
                [STBellManager manager].player = _player;
            }
            
            
            
            [_player prepareToPlay];
            [_player play];
            return;
        }
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundId);
        AudioServicesPlaySystemSound(soundId);
        return;
    }

}

//停止播放
+ (void)stopBell:(NSString *)sound system:(BOOL)isSystem {

    NSArray <NSString *>*soundArr = [sound componentsSeparatedByString:@"."];
//    NSString * soundName = soundArr.firstObject;
    NSString * soundType = soundArr.lastObject;
    SystemSoundID soundId = [sound intValue];
    
    if (isSystem) {
        AudioServicesDisposeSystemSoundID(soundId);
    } else if ([soundType isEqualToString:@"mp3"]) {
        if ([STBellManager manager].player) {
            [[STBellManager manager].player stop];
            [STBellManager manager].player = nil;
        }
    }
}


@end
