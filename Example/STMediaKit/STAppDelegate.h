//
//  STAppDelegate.h
//  STMediaKit
//
//  Created by shilei2015 on 07/28/2023.
//  Copyright (c) 2023 shilei2015. All rights reserved.
//

@import UIKit;

@interface STAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
